public class IsLeapYear{
	public String determine(int i){
		if(i%400==0)
			return "true";
		else if( (i%4==0) && (i%100!=0) )
			return "true";
		else
			return "false";
	}
}