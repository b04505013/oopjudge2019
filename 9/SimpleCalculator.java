
import java.text.DecimalFormat;
/**
 * 
 * @author B04505013
 * a class for a simple calculator with defined exception
 */
public class SimpleCalculator {
	private double result = (double)(float)((double)(0.0f));
	int state = 0; // 0 is off     1 is on    2 is after one calc
	int count;
	String[] op;
	
	/**
	 * 
	 * @param cmd
	 * cmd is string containing a operator and a value separated by a space
	 * @throws UnknownCmdException
	 * throws defined exception to UnknownCmdException
	 */
	public void calResult(String cmd) throws UnknownCmdException{
		String[] f = cmd.split(" ");
		op = f;
		double i;
		
		if(f.length != 2)
			throw new UnknownCmdException("Please enter 1 operator and 1 value separated by 1 space");
		if(!f[0].equals("+") && !f[0].equals("-") && !f[0].equals("*") && !f[0].equals("/")) {
			try {
				i = Double.parseDouble(f[1]);
			}
			catch(NumberFormatException e) {
				throw new UnknownCmdException(f[0] +" is an unknown operator and "+f[1]+" is an unknown value");
			}
			throw new UnknownCmdException(f[0] +" is an unknown operator");
		}
		
		try {
			i = Double.parseDouble(f[1]);
		}
		catch(NumberFormatException e){
			throw new UnknownCmdException(f[1]+" is an unknown value");
		}
		
		if(Double.parseDouble(f[1]) == 0) {
			throw new UnknownCmdException("Can not divide by 0");
		}
		switch(f[0]) {
		case "+":
			result = result + i;
			break;
		case "-":
			result -= i;
			break;
		case "*":
			result *= i;
			break;
		case "/":
			result /= i;
			break;
		}
		count++;
		state = 1;
	}
	
	/**
	 * 
	 * @return
	 * return the state just happened
	 */
	public String getMsg() {
		if(count == 0) {
			state = 1;
			return "Calculator is on. Result = 0.00";
		}
		if(state == 1) {
			if(count == 1) {
				state = 2; 
				return "Result " + op[0] + " " + trim(Double.parseDouble(op[1])) +" = "+ trim(result) +". New result = " + trim(result);
			}
			if(count > 1) {
				state = 2;
				return "Result "+ op[0] +  " " + trim(Double.parseDouble(op[1]))+ " = "+trim(result) +". Updated result = "+trim(result);
			}
		}
		if(state == 2)
			return "Final result = "+ trim(result);
		return "";
	}
	
	/**
	 * 
	 * @param cmd
	 * cmd is to determine whether the calculation comes to the end
	 * @return
	 * return true if yes
	 * return false if not
	 */
	public boolean endCalc(String cmd) {
		if(cmd.equals("r") || cmd.equals("R"))
			return true;
		return false;
	}
	
	/**
	 * 
	 * @param rough
	 * a rough double number
	 * @return
	 * return a trimmed number with two float point numbers
	 */
	public static String trim(double rough) {  
        return new DecimalFormat("0.00").format(rough);//turn trimmed string number to double
    }
	
	public static void main(String[] args) {
		SimpleCalculator cal = new SimpleCalculator();
		String cmd = null;
		System.out.println(cal.getMsg());
		String cmd_str = "+ 5,- 2,* 5,/ 3,% 2,* D,X D,XD,, ,/ 1000000,/ 00.000,/ 0.000001,+ 1 + 1,- 1.66633,r R,r";
		String[] cmd_arr = cmd_str.split(",");
		for (int i = 0; i < cmd_arr.length; i++) {
		  try {
		    if (cal.endCalc(cmd_arr[i])) 
		      break;		
		    cal.calResult(cmd_arr[i]);
		    System.out.println(cal.getMsg());
		  } catch (UnknownCmdException e) {
		    System.out.println(e.getMessage());
		  }
		}
		System.out.println(cal.getMsg());
	}
}
