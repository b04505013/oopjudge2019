
/**
 * 
 * @author B04505013
 * exception class
 */
public class UnknownCmdException extends Exception {
	public UnknownCmdException(String s) {
		super(s);
	}
}
