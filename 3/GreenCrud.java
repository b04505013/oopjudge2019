/**
 * @author b04505013
 */

public class GreenCrud {

	public int calPopulation(int i, int d) {
		int times = d / 5;

		if (times == 0)
			return i;
		if (times == 1)
			return i;

		int f1 = 0;
		int f2 = 1;
		for (int j = 0; j < times; j++) {
			f2 = f1 + f2;
			f1 = f2 - f1;
		}
		return f2 * i;

	}

}
