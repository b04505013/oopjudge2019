/**
 * @author b04505013
 * Pizza is a class to generate a pizza object
 * with parameters of size, number of cheese, number of pepperoni,
 * number of ham
 */
public class Pizza {
	private String size;
	private int numofcheese;
	private int numofpep;
	private int numofham;
	//a object Pizza set with value
	public Pizza(String a, int b, int c, int d) {
		size = a;
		numofcheese = b;
		numofpep = c;
		numofham = d;
	}
	//default constructor
	public Pizza() {
		size = "small";
		numofcheese = 1;
		numofpep = 1;
		numofham = 1;
	}

	
	//setting values of a Pizza
	public void setSize(String x) {
		size = x;
	}

	public void setNumberOfCheese(int x) {
		numofcheese = x;
	}

	public void setNumberOfPepperoni(int x) {
		numofpep = x;
	}

	public void setNumberOfHam(int x) {
		numofham = x;
	}

	//to return the specific value of the Pizza
	public String getSize() {
		return size;
	}

	public int getNumberOfCheese() {
		return numofcheese;
	}

	public int getNumberOfPepperoni() {
		return numofpep;
	}

	public int getNumberOfHam() {
		return numofham;
	}

	//to return how much a Pizza costs
	public double calcCost() {
		if (size == "small") {
			return 10 + (numofcheese + numofpep + numofham) * 2;
		}
		if (size == "medium") {
			return 12 + (numofcheese + numofpep + numofham) * 2;
		}
		if (size == "large") {
			return 14 + (numofcheese + numofpep + numofham) * 2;
		}
		return 0;
	}

	//to return the value of parameters by String
	public String toString() {
		return "size = " + size + ", numOfCheese = " + numofcheese + ", numOfPepperoni = " + numofpep + ", numOfHam = "
				+ numofham;
	}
	//to detect if the two Pizza have the same value
	public boolean equals(Pizza a) {
		if (a.size == size && a.numofcheese == numofcheese && a.numofham == numofham && a.numofpep == numofpep) {
			return true;
		}
		return false;
	}

}
