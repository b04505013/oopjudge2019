
/**
 * @author B04505013
 *  this is a class used to order Pizza, and calculate the total money.
 *  It operates with class Pizza
 */
public class PizzaOrder {
	//create parameters
	int numberPizzas;
	private Pizza p1 = new Pizza();
	private Pizza p2 = new Pizza();
	private Pizza p3 = new Pizza();
	
	//a boolean function that set the number of pizza ordered,
	//and also return if it's valid
	public boolean setNumberPizzas(int numberPizzas) {
		this.numberPizzas = numberPizzas;
		if (numberPizzas <= 3 && numberPizzas >= 1) {
			return true;
		}
		else return false;
	}
	
	//set the value of parameters
	public void setPizza1(Pizza a) {
		p1 = a;
	}

	public void setPizza2(Pizza b) {
		p2 = b;
	}

	public void setPizza3(Pizza c) {
		p3 = c;
	}
	
	//calculate the total money of ordered pizzas
	public double calcTotal() {
		if(numberPizzas == 1) return p1.calcCost();
		if(numberPizzas == 2) return p1.calcCost() + p2.calcCost();
		return p1.calcCost() + p2.calcCost() + p3.calcCost();
	}
	
	public static void main(String[] args) {
		Pizza pizza1 = new Pizza("large", 1, 0, 1);
		Pizza pizza2 = new Pizza("medium", 2, 2, 5);
		Pizza pizza3 = new Pizza();
		PizzaOrder order = new PizzaOrder();
		System.out.println(order.setNumberPizzas(5));
		order.setNumberPizzas(2);
		order.setPizza1(pizza1);
		order.setPizza2(pizza2);
		System.out.println(order.calcTotal());
		order.setNumberPizzas(3);
		order.setPizza1(pizza1);
		order.setPizza2(pizza2);
		order.setPizza3(pizza3);
		System.out.println(order.calcTotal());
	}

}
