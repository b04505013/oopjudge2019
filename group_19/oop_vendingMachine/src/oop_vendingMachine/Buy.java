package oop_vendingMachine;

import java.util.Scanner;

public class Buy {
	private int insertedDollars;
	private int change;
	private int[] changeCoins;
	private String dataIn;
	private product[] availableProduct;
	private product productSelected;
	private boolean productSelectFlag = false;
	Scanner scanner = new Scanner(System.in);

	public void buy() {
		System.out.println("Please Insert Coins...");
		insertedDollars += scanner.nextInt();
		while (true) {
			System.out.println("$" + insertedDollars + " NTD Inserted.");
			availableProduct = checkAvailableProduct(insertedDollars);
			showAvailableProduct(availableProduct);
			System.out.println("Please Key '-1' to contiune Insert Coins...");
			System.out.println("Please Key '-2' for refund...");
			dataIn = scanner.next();
			if (dataIn.equals("-1")) {
				// insert more money
				System.out.println("Please Insert Coins...");
				insertedDollars += scanner.nextInt();
				continue;
			}else if (dataIn.equals("-2")) {
				// refund
				System.out.println("Refund "+ insertedDollars + "dollars.");
				break;
			}else {
				// select product
				try {
					productSelected = selectProduct(dataIn,insertedDollars);
					productSelectFlag = true;
				} catch (productSelectException e) {
					System.out.println("something went wrong");
				}
			}
			if (productSelectFlag == true)
				break;
		}
		//give change
		change = insertedDollars;
		if(productSelectFlag == true)
			change = insertedDollars - productSelected.getPrice();
		System.out.println("return change:");
		changeCoins = change(change);
		System.out.println("50 dollar:" + changeCoins[0]);
		System.out.println("10 dollar:" + changeCoins[1]);
		System.out.println("5 dollar:" + changeCoins[2]);
		System.out.println("1 dollar:" + changeCoins[3]);

	}

	private void showAvailableProduct(product[] products) {
		// TODO Auto-generated method stub
		if(products[0] != null)
			System.out.println("Please Choose one:");
		for(int i=0;i<products.length;i++) {
			if(products[i] ==null) {
				break;
			}
			System.out.println(products[i].name() + " \t$" + products[i].getPrice()+ " \tNTD");
		}
	}

	private product[] checkAvailableProduct(int money) {
		// TODO Auto-generated method stub
		product[] products = new product[100];
		int count = 0;
		for (product p : product.values()) {
			if(p.getPrice()<money&&p.getAmount()>0) {
				products[count] = p;
				count++;
			}
		}
		return products;
	}

	public product selectProduct(String s, int inserted_dollors) throws productSelectException {
		product selectProduct;
		for (product p : product.values()) {
			if (p.name().equals(s)) {
				selectProduct = p;
				if (selectProduct.getPrice() < inserted_dollors) {
					System.out.println(selectProduct.toString() + " selected.");
					return selectProduct;
				}else {
					throw new productSelectException();
				}
			}
		}
		throw new productSelectException();
	}
	public int[] change(int money) {
		int[] coins = new int[4];
		coins[0] = money/50;
		money%=50;
		
		coins[1] = money/10;
		money%=10;
		
		coins[2] = money/=5;
		money%=5;
		
		coins[3] = money;
		return coins;
	}
}
