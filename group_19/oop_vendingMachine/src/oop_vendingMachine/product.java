package oop_vendingMachine;

public enum product {
	COKE,TEA,COFFEE;
	
	private int price;
	private int amount;
	
	public int getPrice() {
		return price;
	}
	public int getAmount() {
		return amount;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	public void setAmount(int amount) {
		this.amount = amount;
	}
	
	
}
