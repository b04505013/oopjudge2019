package oop_hotel;

public class Hotel {

	
	private int HotelID;
    private int HotelStar;
    private String Locality;
    private String Address;
    private roomType RoomType;
    private roomType RoomType2;

    public Hotel(int id,int star,String lo,String add ) {
		int hotelid;
		hotelid=id;
		this.HotelID=id;
		int hotelstar;
		hotelstar=star;
		this.HotelStar=star;
		String locality;
		locality=lo;
		this.Locality=lo;
		String streetaddress;
		streetaddress=add;
		this.Address=add;
	}
    
	public int getHotelID() {
		return HotelID;
	}


	public int getHotelStar() {
		return HotelStar;
	}


	public String getLocality() {
		return Locality;
	}


	public String getAddress() {
		return Address;
	}
	
	public roomType getRoomType() {
		return RoomType;
	}

	public void setHotelID(int hotelID) {
		HotelID = hotelID;
	}


	public void setHotelStar(int hotelStar) {
		HotelStar = hotelStar;
	}


	public void setLocality(String locality) {
		Locality = locality;
	}


	public void setAddress(String address) {
		Address = address;
	}


	public void setRoomType(roomType roomType) {
		RoomType = roomType;
	}

}


