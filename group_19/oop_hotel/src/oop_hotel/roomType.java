package oop_hotel;

import java.time.LocalDate;

public enum roomType {
	SINGLE, DOUBLE;
	
    private int RoomPrice;
    private int[][] Number = new int[3000][365];
    
	public int getRoomPrice() {
		return this.RoomPrice;
	}
	public int getNumber(LocalDate date) {
		int number;
		number =  this.Number[date.getYear()][date.getDayOfYear()];
		return number;
	}
	public void setRoomPrice(int roomPrice) {
		this.RoomPrice = roomPrice;
	}
	public void setNumber(int number, LocalDate date) {
		this.Number[date.getYear()][date.getDayOfYear()] = number;
	}
}
