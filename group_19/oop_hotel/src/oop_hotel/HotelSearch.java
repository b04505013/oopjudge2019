package oop_hotel;

import java.time.LocalDate;
import java.time.Period;

public class HotelSearch {
	public void Search(Hotel[] hotels, LocalDate checkInDate, LocalDate checkOutDate,
			int persons, int numberOfRooms) {
		LocalDate date = checkInDate;
		Period period;
		period = Period.between(checkInDate, checkOutDate);
		boolean checkFlag;
		//Hotel[] availableHotels = new Hotel[1500];
		int numDoubleRoom = persons - numberOfRooms;
		int numSingleRoom = numberOfRooms - numDoubleRoom;
		//check room type and rooms needed
		
		//check room availability
		for(int i=0;i<1500;i++) {
			if(hotels[i] != null) {
				
				checkFlag = true;
				for(int j = 0; j < period.getDays(); j++) {
					if(hotels[i].getRoomType().SINGLE.getNumber(date)< numSingleRoom || hotels[i].getRoomType().DOUBLE.getNumber(date)< numDoubleRoom) {
						checkFlag = false;
						break;
					}
					date.plusDays(1);
				}
				if(checkFlag == true) {
					System.out.println("Hotel " +hotels[i].getHotelID() + ", " + hotels[i].getHotelStar() + "Stars.");
					System.out.println("Single: " + numSingleRoom + "Rooms.");
					System.out.println("Double: " + numDoubleRoom + "Rooms.");
					System.out.println("Total: $" + numSingleRoom*hotels[i].getRoomType().SINGLE.getRoomPrice()+numDoubleRoom*hotels[i].getRoomType().DOUBLE.getRoomPrice() + "NTD.");
				}
			}
		}
	}

}