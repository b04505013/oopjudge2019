package oop_hotel;

import java.time.LocalDate;
import java.time.Period;

public class HotelReservation {

	private static Period period;// = new Period();

	// a object Reservation set with value
	public void Reservation(String userID, Hotel hotel, LocalDate checkInDate, LocalDate checkOutDate,
			roomType roomtype, int numberOfRooms) {
		HotelReservation res = new HotelReservation();
		try {
			res.checkIsAvaliableReservation(hotel, checkInDate, checkOutDate, roomtype, numberOfRooms);
		} catch (HotelReservationException ex) {
			System.out.println(ex.getMessage());
		}

		changeRoomNumbers(hotel, checkInDate, checkOutDate, roomtype, numberOfRooms);
		
		
		System.out.println("Reservation success.");
		System.out.println("Reservation Code: ");
		System.out.println("User ID: " + userID);
		System.out.println("Hotel Number: " + hotel.getHotelID());
		System.out.println("Room Type: " + roomtype);
		System.out.println("Check In and Out Date: " + checkInDate + "/ " + checkOutDate);
		period = Period.between(checkInDate, checkOutDate);
		System.out.println("Stay for " + period.getDays() + "Nights");

		System.out.println("Total Price " + roomtype.getRoomPrice() * period.getDays());

	}

	private void changeRoomNumbers(Hotel hotel, LocalDate checkInDate, LocalDate checkOutDate, roomType roomtype,
			int numberOfRooms) {
		period = Period.between(checkInDate, checkOutDate);
		LocalDate date = checkInDate;
		
		for (int i = 0; i < period.getDays(); i++) {
			hotel.getRoomType().setNumber(hotel.getRoomType().getNumber(date)-numberOfRooms,date);
			date.plusDays(1);
		}
	}

	private void checkIsAvaliableReservation(Hotel hotel, LocalDate checkInDate, LocalDate checkOutDate,
			roomType roomtype, int numberOfRooms) throws HotelReservationException {
		boolean checkFlag = true;
		// checkin is before checkout
		checkFlag = checkOutDate.isAfter(checkInDate);
		if (checkFlag == false) {
			throw new HotelReservationException();
		}
		// room is available
		period = Period.between(checkInDate, checkOutDate);
		LocalDate date = checkInDate;
		for (int i = 0; i < period.getDays(); i++) {
			if (roomtype.getNumber(date) > numberOfRooms) {
				checkFlag = true;
			} else {
				checkFlag = false;
				throw new HotelReservationException();
			}
			date.plusDays(1);
		}
	}

}
