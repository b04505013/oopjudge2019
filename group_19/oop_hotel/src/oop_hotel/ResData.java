package oop_hotel;

import java.time.LocalDate;

public class ResData {
	private Hotel hotel;
	private LocalDate checkInDate;
	private LocalDate checkOutDate;
	private roomType roomtype;
	private int NumberOfRooms;
	public Hotel getHotel() {
		return hotel;
	}
	public LocalDate getCheckInDate() {
		return checkInDate;
	}
	public LocalDate getCheckOutDate() {
		return checkOutDate;
	}
	public roomType getRoomtype() {
		return roomtype;
	}
	public int getNumberOfRooms() {
		return NumberOfRooms;
	}
	public void setHotel(Hotel hotel) {
		this.hotel = hotel;
	}
	public void setCheckInDate(LocalDate checkInDate) {
		this.checkInDate = checkInDate;
	}
	public void setCheckOutDate(LocalDate checkOutDate) {
		this.checkOutDate = checkOutDate;
	}
	public void setRoomtype(roomType roomtype) {
		this.roomtype = roomtype;
	}
	public void setNumberOfRooms(int numberOfRooms) {
		NumberOfRooms = numberOfRooms;
	}
}
