
public class SimpleArrayList {
	
	private Integer a[];
	
	public SimpleArrayList() {
		a = new Integer[0];
	}
	
	public SimpleArrayList(int n) {
		a = new Integer[n];
		for(int i = 0; i < n; i++) {
			a[i] = new Integer(0);
		}
	}
	
	public void add(Integer i) {
		if(i == null) {
			Integer temp[] = new Integer[a.length + 1];
			for(int j = 0; j < a.length; j++) {
				temp[j] = a[j];
			}
			temp[a.length] = null;
			a = temp;
			
			return;
		}
		
		Integer temp[] = new Integer[a.length + 1];
		for(int j = 0; j < a.length; j++) {
			temp[j] = a[j];
		}
		temp[a.length] = new Integer(i);
		a = temp;
	}
	
	public Integer get(int index) {
		if(index > a.length - 1)return null;
		return a[index];
	}
	
	public Integer set(int index, Integer element) {
		if(index > a.length - 1) {
			return null;
		}
		Integer temp = a[index];
		a[index] = element;
		return temp;
	}
	
	public boolean remove(int index) {
		if(index > a.length - 1)return false;
		if(a[index] == null)return false;
		
		Integer temp[] = new Integer[a.length - 1];    //remove a[index]
		for(int i = index; i < a.length - 1; i++) {
			a[i] = a[i + 1];
		}
		
		for(int i = 0; i < temp.length ; i++) {   //let temp become modified a
			temp[i] = a[i];
		}
		a = temp;
		
		return true;
	}
	
	public void clear() {
		a = new Integer[0];
	}
	
	public int size() {
		return a.length;
	}
	
	public boolean retainAll(SimpleArrayList l) {
		SimpleArrayList newa = new SimpleArrayList();
		
		for(int i = 0; i<a.length; i++) {
			for(int j = 0; j < l.a.length; j++) {
				if(a[i].equals(l.a[j])) {
					newa.add(a[i]);
				}
			}
		}
		Integer b[] = a;                         //  b = original a
		a = newa.a;
		
		if(b.length == newa.a.length)return false;
		return true;
	}
	
	public static void main(String[] arg) {
		System.out.println("=== TASK 1 ===");
		SimpleArrayList list = new SimpleArrayList();
		System.out.println(list.get(0));

		System.out.println("=== TASK 2 ===");
		list.add(2);
		list.add(5);
		list.add(8);
		list.add(1);
		list.add(12);
		System.out.println(list.get(2));

		System.out.println("=== TASK 3 ===");
		System.out.println(list.get(5));

		System.out.println("=== TASK 4 ===");
		System.out.println(list.set(2, 100));

		System.out.println("=== TASK 5 ===");
		System.out.println(list.get(2));

		System.out.println("=== TASK 6 ===");
		System.out.println(list.set(5, 100));

		System.out.println("=== TASK 7 ===");
		System.out.println(list.remove(2));

		System.out.println("=== TASK 8 ===");
		System.out.println(list.get(2));

		System.out.println("=== TASK 9 ===");
		System.out.println(list.remove(2));

		System.out.println("=== TASK 10 ===");
		System.out.println(list.get(2));

		System.out.println("=== TASK 11 ===");
		System.out.println(list.get(3));

		System.out.println("=== TASK 12 ===");
		list.clear();
		System.out.println(list.get(0));

		System.out.println("=== TASK 13 ===");
		SimpleArrayList list2 = new SimpleArrayList(5);
		System.out.println(list2.get(3));

		System.out.println("=== TASK 14 ===");
		System.out.println(list2.get(9));

		System.out.println("=== TASK 15 ===");
		for (int i = 0; i < list2.size(); i++) {
			System.out.println(list2.set(i, i));
		}
		for (int i = 0; i < 5; i++) {
			list.add(i);
		}
		System.out.println(list.retainAll(list2));

		System.out.println("=== TASK 16 ===");
		for (int i = 0; i < list.size(); i++) {
			System.out.println(list.get(i));
		}

		System.out.println("=== TASK 17 ===");
		System.out.println(list2.remove(0));
		System.out.println(list2.remove(2));
		System.out.println(list.retainAll(list2));

		System.out.println("=== TASK 18 ===");
		for (int i = 0; i < list.size(); i++) {
			System.out.println(list.get(i));
		}

		System.out.println("=== TASK 19 ===");
		System.out.println(list.set(1, null));
		System.out.println(list.remove(1));

		System.out.println("=== TASK 20 ===");
		for (int i = 0; i < list.size(); i++) {
			System.out.println(list.get(i));
		}

		System.out.println("=== TASK 21 ===");
		System.out.println(list.set(1, 123));

		System.out.println("=== TASK 22 ===");
		for (int i = 0; i < list.size(); i++) {
			System.out.println(list.get(i));
		}

		System.out.println("=== TASK 23 ===");
		System.out.println(list.remove(1));

		System.out.println("=== TASK 24 ===");
		for (int i = 0; i < list.size(); i++) {
			System.out.println(list.get(i));
		}

		System.out.println("=== TASK 25 ===");
		list.add(null);
		System.out.println(list.remove(2));

		System.out.println("=== TASK 26 ===");
		for (int i = 0; i < list.size(); i++) {
			System.out.println(list.get(i));
		}
	}
}
