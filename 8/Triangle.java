
public class Triangle extends Shape {

	public Triangle(double length) {
		super(length);
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public void setLength(double length) {
		this.length = length;
	}

	@Override
	public double getArea() {
		return ShapeFactory.trimfloat(0.43301 * length * length);
	}

	@Override
	public double getPerimeter() {
		return ShapeFactory.trimfloat(3 * length);
	}
	

}
