

public class Circle extends Shape {

	public Circle(double length) {
		super(length);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void setLength(double length) {
		this.length = length;
		
	}

	@Override
	public double getArea() {
		return ShapeFactory.trimfloat(ShapeFactory.pi * (this.length / 2) * (length / 2));
	}

	@Override
	public double getPerimeter() {
		return ShapeFactory.trimfloat(ShapeFactory.pi * length);
	}
	

}
