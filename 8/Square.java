
public class Square extends Shape {

	public Square(double length) {
		super(length);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void setLength(double length) {
		this.length = length;
		
	}

	@Override
	public double getArea() {
		return ShapeFactory.trimfloat(length * length);
	}

	@Override
	public double getPerimeter() {
		return ShapeFactory.trimfloat(4 * length);
	}
	

}
