

import java.text.DecimalFormat;

public class ShapeFactory {
	enum Type {	Circle, Triangle, Square }
	
	public static final double pi = 3.1415926;
	
	public Shape createShape(ShapeFactory.Type shapeType, double length) {
		switch(shapeType) {
		case Circle:
			return new Circle(length);
		case Triangle:
			return new Triangle(length);
		case Square:
			return new Square(length);
		}
		return null;
	}
	
	
	public static double trimfloat(double rough) {
		DecimalFormat df = new DecimalFormat("0.00");   
		return Double.parseDouble(df.format(rough));//turn trimmed string number to double
	}
	
	
	public static void main(String[] args) {
		ShapeFactory shapeFactory = new ShapeFactory();
		Shape triangle = shapeFactory.createShape(ShapeFactory.Type.Triangle, 7.5);
		Shape square = shapeFactory.createShape(ShapeFactory.Type.Square, 5);
		Shape circle = shapeFactory.createShape(ShapeFactory.Type.Circle, 5);
		System.out.println(triangle.getInfo());
		System.out.println(square.getInfo());
		System.out.println(circle.getInfo());
		System.out.println(square.getArea() > triangle.getArea());
		System.out.println(square.getPerimeter() > circle.getPerimeter());
		triangle.setLength(10.5);
		square.setLength(3.2);
		circle.setLength(0);
		System.out.println(triangle.getInfo());
		System.out.println(square.getInfo());
		System.out.println(circle.getInfo());
		System.out.println(square.getArea() > triangle.getArea());
		System.out.println(square.getPerimeter() > circle.getPerimeter());
	}
}
