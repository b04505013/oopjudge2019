
/**
 * a class to store exceptions from Simple_ATM.
 * @author B04505013
 *
 */
public class ATM_Exception extends Exception{
	private ExceptionTYPE exceptioncondition;
	
	public enum ExceptionTYPE{
		BALANCE_NOT_ENOUGH,
		AMOUNT_INVALID
	}
	
	public ATM_Exception(ExceptionTYPE t) {
		super(t.toString());
		exceptioncondition = t;
	}
}
