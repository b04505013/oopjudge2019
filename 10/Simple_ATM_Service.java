/**
 * 
 * @author b04505013
 *	Simple_ATM_Service is a class with three method to implement ATM service.
 */
public class Simple_ATM_Service implements ATM_Service {
	
	/**
	 * to check if the money is enough in the account
	 * 
	 * @param account
	 * a account will be checked
	 * @param money
	 * the money you want to withdraw
	 * @throws ATM_Exception
	 * throws defined exception to ATM_Exception
	 * @return
	 * return true if the money in the account is enough.
	 */
	public boolean checkBalance(Account account, int money) throws ATM_Exception {
		// TODO Auto-generated method stub
		if(account.getBalance() >= money) {
			return true;		
			}
		throw new ATM_Exception(ATM_Exception.ExceptionTYPE.BALANCE_NOT_ENOUGH);
	}

	@Override
	/**
	 * to check if the amount of money you want to withdraw is valid
	 * 
	 * @param money
	 * the money you want to withdraw
	 * @throws ATM_Exception
	 * throws defined exception to ATM_Exception
	 * @return
	 * return true if the money can be divided by 1000.
	 */
	public boolean isValidAmount(int money) throws ATM_Exception {
		// TODO Auto-generated method stub
		if(money%1000 == 0) {
			return true;
		}
		throw new ATM_Exception(ATM_Exception.ExceptionTYPE.AMOUNT_INVALID);
	}

	@Override
	/**
	 * to withdraw money. first check the account by the two method above
	 * 
	 * @param account
	 * a account will be checked
	 * @param money
	 * the money you want to withdraw
	 * @throws ATM_Exception
	 * throws defined exception to ATM_Exception
	 */
	public void withdraw(Account account, int money) {
		// TODO Auto-generated method stub
		Simple_ATM_Service s = new Simple_ATM_Service();
		try {
			s.checkBalance(account, money);
			s.isValidAmount(money);
			account.setBalance(account.getBalance() - money);
		} catch (ATM_Exception e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
			System.out.println(e.getMessage());
		}
		System.out.println("updated balance : " + account.getBalance());
		
	}
	
	public static void main(String[] args) {
		Account David = new Account(4000);
		Simple_ATM_Service atm = new Simple_ATM_Service();
		System.out.println("---- first withdraw ----");
		atm.withdraw(David,1000);
		System.out.println("---- second withdraw ----");
		atm.withdraw(David,1000);
		System.out.println("---- third withdraw ----");
		atm.withdraw(David,1001);
		System.out.println("---- fourth withdraw ----");
		atm.withdraw(David,4000);
	}

}
